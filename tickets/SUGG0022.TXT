===========================================================================
 BBS: SECRET C::The C Source*HST DS*(403)256-7
Date: 10-15-93 (22:24)             Number: 98
From: PAUL SIDORSKY                Refer#: NONE
  To: SOLEIL LAPIERRE               Recvd: NO  
Subj: Inf 1.2                        Conf: (7) General
---------------------------------------------------------------------------
I finally had the chance to playtest the new version (as you could guess by
the time this message is written...late night is my only spare time now, it
seems).

MUCH MUCH _MUCH_ better on the play control.  I can now nearly triple the
speed at which I play the game, and that makes it a) less boring, and b)
more playable (i.e. I'll play more games now since it takes less time).
Good show!

And I notice the "digging underneath the agent" has been fixed, which is
good, though I admit it screwed me up a couple of times until I realized
what was going on.  Fortunately, they weren't fatal mistakes, just
time-consuming ones.

I saw some of those Poles around.  Neat, though they mostly seem to be for
decoration, and to show off.  :-)  They remind me of my days as a (younger)
kid at the playground.  <grin>  When does the first Door Switch come in?

One minor gripe...the speed-changing keys.  I should think one would expect
to press + to INCREASE (+, add, get the connection?  <grin>) speed, and -
to Decrease it.  I'm assuming it works the other way around because it's
the delay you add to.  I dunno.  Either swap the keys, or change the docs
so it's described more accurately.

Oh BTW, you have a small typo in the history file:

 - Fixed a minor oversight in the START program; Wome windows were not being
   erased before the main menu was drawn.

See if you can guess.  :-)  Actually, I think it SOUNDS neat that way, but
it makes no sense.  Also, there are still mentions of INFILT11.EXE around.
I forget where, but I saw at least one.

I'll have to get my mom to try it sometimes.  Then we can get another
outside opinion about the play control.  Will let you know when that
happens.

BTW, what's VGACAD like?  I'm pretty impressed with Raster Master, so I
don't think I'll change over, but I'm just wondering if it supports
anything extra-special.

Oh, I haven't forgot about registering...I'll just be delayed a bit (have
to get something more important).  You'll probably get it in about one
month from now, in mid-November.

Long message, but under the 60-line "courtesy limit".  That's important,
since I didn't make this Private this time (I figure other people might
want to hear what I have to say.  <grin>).


--- Blue Wave/Max v2.12 [NR]
 * Origin: SECRET C::The C Source*HST DS*(403)256-7019,Calgary,Ab (1:134/21)
