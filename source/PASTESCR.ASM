_AOFF          EQU     14d

PASTESCREEN_TEXT SEGMENT BYTE PUBLIC 'CODE'
                 ASSUME CS:PASTESCREEN_TEXT,DS:_DATA


; This function copies 64000 bytes to the screen memory at A0000.
; Called as: PasteScreen(unsigned seg, unsigned off);
;

               PUBLIC  _PasteScreen
_PasteScreen   PROC    FAR

               PUSH    BP
               PUSH    CX
               PUSH    SI
               PUSH    ES
               PUSH    DI
               MOV     BP,SP
               MOV     SI,[BP+_AOFF+2]
               MOV     DS,[BP+_AOFF+0]       ;SOURCE
               MOV     DI,0
               MOV     CX,0A000h
               MOV     ES,CX                 ;DEST
               MOV     CX,32000d             ;Count - # bytes to do

               CLD

               REP     MOVSW

               MOV     CX,_DATA
               MOV     DS,CX
               POP     DI
               POP     ES
               POP     SI
               POP     CX
               POP     BP
               RET
_PasteScreen   ENDP

PASTESCREEN_TEXT ENDS

DGROUP         GROUP   _DATA,_BSS

_DATA          SEGMENT WORD PUBLIC 'DATA'

_DATA          ENDS

_BSS           SEGMENT WORD PUBLIC 'BSS'

_BSS           ENDS
               END

