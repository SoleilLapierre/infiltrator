Modem play, si'l vout plais. :)

Have a contest for registered users - best levels created w/the level editor.
The best are combined into Infiltraitor: Special Edition,
free to reg'd users, $20 otherwise.

Random chance of dropping some on yourself - new way to die.

How about compiling GO.BAT into a standalone COM with PC Mag's BAT2EXEC?

Check fails to check for extra files in it's dir.

I was also able to hexedit CHECK.EXE.  I have some PD code
for internal CRCs. Want?

READ is *very* slow IMO.  How about doing something like
the following for every 23 lines of text? Sorry, you'll
have to translate:

BTW, I found a bug...try this
Do H)igh Scores or P)ractice Level, press esc to back out of it, and watch
the background.

I only have one comment so far... I own LodeRunner for the PC, and have
gotten so I easily complete all 99 levels. The problem I have with your
game is the way the "Dig" keys work. In LodeRunner, when you "Dig" to your
left or right, the character stops moving, just as if you pressed any other
key other than an arrow key. In your game, any other key than a direction key
stops the character from moving, EXCEPT the "Dig" keys. This makes your game
very difficult for me to play.
 I know you probably were not looking to make a "clone" of
the LodeRunner game, but if there were some way to change
this, I would very much appreciate it.

There is one thing in the play control I find kind of tricky, and that is
after you press a key, the thing keeps going and going and going.  It is
tough to learn to hit the spacebar, THEN dig, rather than just releasing
the joystick and hitting the button like I was able to do in Lode Runner.
I am getting used to it now, but it was tricky at first.  I'm not
suggesting that you change this to copy Lode Runner, but I just thought I'd
let you know it took some getting used to.

I have a suggestion for something new...something like a JetPack that would
allow you to control your falls for a period of time.  It would make for an
interesting addition.  This I'm not taking from Lode Runner; it's my own
idea.  Oh, and also SoundBlaster support would be nice (we SB people get
kind of picky when we have to listen to the PC Speaker.  <grin>).

I downloaded it from Techtalk, and its a great game, but on
my VGA screen the little guy with the blue background (the
first level, always die when I have the last treasure to
go, the background is the walls) makes it harder to play,
because for some reason the little guy blends in very
nicely. It is a great game, I like it!

 Well I wrote another message, but I have to tell you the program
 doesn't SAVE my games. I tried typing "S" and "s" and it never
 saved my game, and I passed the first level! Plus did you know
 when you press up ( 8 on the numeric keypad) and there is no
 ladder your guy stops ( made the game easier to play! ). Also
 the delay for the scrolling screens is a bit to slow ( maybe
 only on my computer, I have a 20 mhz 386 ).

 Your "READ" is slow, maybe you should get someone who has
 Turbo Pascal 6 and above to re-write it for you.... Also I
 had two ideas, make each of your executable files have a
 self test, that way you don't have to run check all the time.
 Also add a color control, make the colors bright and dark.

I suppose doing SoundBlaster support's out of the question?  :-(

I let my son play infiltrator today and the biggest observation I would have,
is that he was unable to play it at all. His hand-eye
coordination is not what mine is and he was unable to even
get the little fella onto a ladder. I helped him there and
the first thing he did was run him off and back to the
bottom. It would help a lot better if he stopped when you
took your hand off the arrow key. Btw, he's only 5, 6 in
Jan. and consistently beats me in Nintendo. He's good, but
this was a high diffucult level for him.

One other not on Infiltrator, please change your go.bat to
run.bat. It confilist with a lot of other software and/or
aliases. A lot of people use go as an alias (via 4DOS or
DOSKEY) for their favorite directory changer, like NCD.
Much easier to type GO.

Umm... I hope you understand what I meant...
I wasn't suggesting that the player should hold the arrow keys to
keep the character moving, just that if any key OTHER than an arrow key
is pressed, the character stops moving. i.e. A "dig" key is pressed, the
character digs a hole, and stops beside it. (instead of walking into his
own trap, as it is now)

I don't know.  On the Atari XE), you held down the button and pressed Left
or Right depending on which way you wanted to dig.  It seems kind of
awkward, but it worked quite well for the standard "Single Button In The
Upper Left Corner" joystick.  Perhaps you could have an option to toggle
between that and using the Left and Right buttons, depending on what type
of joystick the person has.

Another problem I have with Inf is the fact you can dig under yourself, if
you time it right.  This is VERY annoying.  You shouldn't be able to dig
under any space that you are touching, even if just one foot is on it.
I've lost many an agent because of this...even now that I'm used to it, it
still causes problems.

 Hmm, well the little tiny guy on level 0, when he gets stuck in-between
 blocks (what I called the background) its hard to notice him, but all
 the other levels are fine. When my brother saw the game, he didn't like
 the first level he said the first level makes people not want to play
 because it only has those colors, but he liked the other levels, so he
 told me to tell you redo the first level, so more people keep playing.

 I figured out why I died on the last treasure, it seems your program
 wasn't fully compatible with my home-made tsr's, so don't worry
 I fixed the problem with my tsr's, your program is fine.

 I thought of something, why don't you have 2 options in the configure
 menu one to make the guy keep moving when you release a key, and
 another to make the guy stop.

 I also thought of something else, make your programs run a self test
 the first time, and then to run a self test after that, than you use
 check.

 Hmm, well your right.... Well I thought of something that you should do,
 get a few volunteer artists to help you create your graphical screens,
 thann you will have time to touch up your source code.

 You should also release a shareware version of check, all programmers
 could use it, plus you get more money. Check is a excellent utility!

 Can I copy your idea of putting a program like CHECK in your
 shareware stuff??
 ( I made one for home use use � instead of pass, and X instead
 of failed, but it only does CRC calculations)

Why is the new Infiltrator in different colors?  Is it because I missed a
file, or did you change the colors?  Me, I like the old ones better.

Where are these new features implemented?  Did you change a few levels, or
are you just going to add them to new ones?

Finally, 15 GUARDS MAX?!?!?  You must be some kind of sadistic fool!  :-)

MUCH MUCH _MUCH_ better on the play control.  I can now nearly triple the
speed at which I play the game, and that makes it a) less boring, and b)
more playable (i.e. I'll play more games now since it takes less time).
Good show!

And I notice the "digging underneath the agent" has been fixed, which is
good, though I admit it screwed me up a couple of times until I realized
what was going on.  Fortunately, they weren't fatal mistakes, just
time-consuming ones.

I saw some of those Poles around.  Neat, though they mostly seem to be for
decoration, and to show off.  :-)  They remind me of my days as a (younger)
kid at the playground.  <grin>  When does the first Door Switch come in?

One minor gripe...the speed-changing keys.  I should think one would expect
to press + to INCREASE (+, add, get the connection?  <grin>) speed, and -
to Decrease it.  I'm assuming it works the other way around because it's
the delay you add to.  I dunno.  Either swap the keys, or change the docs
so it's described more accurately.

Oh BTW, you have a small typo in the history file:

 - Fixed a minor oversight in the START program; Wome windows were not being
   erased before the main menu was drawn.

See if you can guess.  :-)  Actually, I think it SOUNDS neat that way, but
it makes no sense.  Also, there are still mentions of INFILT11.EXE around.
I forget where, but I saw at least one.

 Dissolving floors under the ladder would come in handy, because if a
 bad agent (the little bad guys) is climbing down a ladder, you could
 have him get stuck, unless you program your agents to be extra smart,
 because some of them fall in the holes (the traps on the levels) by
 themselves. I'm curious, is there a cheat key, or do you have to
 register to get the cheat key, or you haven't made a cheat key at all.

 I was looking at your INF.DOC, and you used the term TSR, but when I
 was browsing through the glossary I never saw TSR, lots of people don't
 know what a TSR is. Why don't you compile "RUNME.BAT" to have extra
 TroubleShooting help, rewrite it in C, or something. I was playing
 "Where in the World is Carmen Sandiego Deluxe" at my friends house, and
 I noticed you don't have any fancy animation or credits. I have a
 suggestion, you should also put something nice saying you
completed a level.
 I have another suggestion, why don't you put a timer so people can see how
 long they've been trying (well that's my brother's suggestion).

 SL> I'll think about it. I'm intending to make the bad guys smarter,
 SL> though.
 You better, they don't pose a threat to my biggest brother.

 Why don't you release CHECK.EXE??

 SL> Credits - that's not a bad idea. There's a "congradulations" screen
 SL> after the last level. What would you suggest for after each level?
 Well you could see this guy (your little good guy, bigger) opening a door
 and getting sucked in, or you see something like floating across the screen,
 and then it says get ready! Anything nice should do.

 SL> No. I dislike games with time limits, and I think with a game like
 SL> this
 SL> people should be able to take as much time as they need. That's why
 SL> you
 SL> can still se the level when you press P)ause - so you can get a good
 SL> look at it.
 I think you misunderstood me, when you quit or something, make the game
 tell the user how long the user was trying.

 I think you have in your INF.DOC the colors of the man are BLUE and RED,
 or something, but I know their wrong.

 Thanks for replying to my input! :-)

 SL> Oh, I see. How about when you exit to DOS it will print "You were
 SL> playing for xx minutes."?

 Yup, like the Norton Utilities.

