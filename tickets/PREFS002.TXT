===========================================================================
 BBS: Tir Nan Og [Calgary, AB]  (403) 274-1741
Date: 12-19-93 (00:17)             Number: 91
From: BRAD SELLICK                 Refer#: 90
  To: SOLEIL LAPIERRE               Recvd: NO  
Subj: PREFERENCES (AGAIN)            Conf: (14) Program
---------------------------------------------------------------------------
SOLEIL LAPIERRE was talking to ALL  about PREFERENCES (AGAIN):

SL>Hi there!

SL>Another question about your preferences in a game:
SL>How do you like your function keys?

SL>There are 3 schemes for selecting special functions that I can think
SL>of:
SL>1) A letter key that corresponds to the first letter of the function,
SL>like (P)ause or (S)ave or (H)elp.

SL>2) A function key for each function following a common layout, like
SL>F1 for help, F2 for save, F10 for quick exit to DOS, etc. Like in
SL>some Apogee or ID games.

SL>3) A menu that pops up when F1 is pressed, from which you can press a
SL>letter to get (H)elp, (S)ave the game, etc. Also like in some Apogee
SL>games.

SL>I'm currently using the first scheme, but I'm wondering if any of you
SL>find that method inconvenient or confusing. If you have a definite
SL>preference for any of the above, please let me know.

SL>Thanks.

I would say, 1 AND 3. the popup help menu would be nice until I learned
the proper keys. And I would prefer intuitive keystrokes to function
keys, any day.

Brad.

FIDONET: 1:134/10
INTERNET: brad.sellick@logical.cuc.ab.ca
COMPUSERVE: 73551,2256
---
 * CmpQwk 1.31 #337 * MilliHelen: Amount of beauty needed to launch one ship.
--- Logical 15.0
 * Origin: Logical Solutions, 24 Lines 299-9900. Region Hub Calgary (1:134/10)
