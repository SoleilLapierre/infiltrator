# Infiltrator

This is an MS-DOS, VGA, PC honker game I wrote in the early 1990s. It's based on Lode Runner but with a few enhancements. 

I tried to sell it as shareware but didn't get too many bites.

The code is absolutely horrible, but that was before I had taken any CS courses or done any professional programming, so slack plz.

More info on this project at [my website](https://www.soleillapierre.ca/projects/software/infilt/).