#ifndef _SDLPCX_H_
#define _SDLPCX_H_

#include <stdio.h>

void ReadPcxLine (char far *, FILE *, unsigned);
void WritePcxLine (char far *, FILE *, unsigned);

#endif