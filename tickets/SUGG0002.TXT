===========================================================================
 BBS: SECRET C::The C Source*HST DS*(403)256-7
Date: 09-23-93 (08:27)             Number: 178
From: ANDREW BROUGHTON             Refer#: NONE
  To: SOLEIL LAPIERRE               Recvd: YES 
Subj: Infiltrator                    Conf: (7) General
---------------------------------------------------------------------------
Hi!

Just D/L'ed your new game. Good job!
I only have one comment so far... I own LodeRunner for the PC, and have
gotten so I easily complete all 99 levels. The problem I have with your
game is the way the "Dig" keys work. In LodeRunner, when you "Dig" to your
left or right, the character stops moving, just as if you pressed any other
key other than an arrow key. In your game, any other key than a direction key
stops the character from moving, EXCEPT the "Dig" keys. This makes your game
very difficult for me to play.
 I know you probably were not looking to make a "clone" of
the LodeRunner game, but if there were some way to change
this, I would very much appreciate it.

Thanks!

-Mr.X


--- Maximus 2.01wb
 * Origin: SECRET C::The C Source*HST DS*(403)256-7019,Calgary,Ab (1:134/21)
