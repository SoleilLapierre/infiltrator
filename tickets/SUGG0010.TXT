===========================================================================
 BBS: SECRET C::The C Source*HST DS*(403)256-7
Date: 09-26-93 (09:59)             Number: 188
From: ANDREW BROUGHTON             Refer#: 98
  To: SOLEIL LAPIERRE               Recvd: NO  
Subj: Re: Infiltrator                Conf: (7) General
---------------------------------------------------------------------------
 -=> Soleil Lapierre was overheard mumbling to Andrew
 -=> Broughton about Infiltrator <=-

 SL> WHAT!! There's a PC version?? If I had known that I wouldn't have
 SL> bothered to write my own. Oh well, I'm glad I did.

Yeah... It's pretty old... From BroderBund (Remember Choplifter?)

 SL> Actually, I just whipped up some code a few days ago that may make
 SL> this possible. I think I'll also make it so that the man stops moving
 SL> if there are no direction keys pressed.

Umm... I hope you understand what I meant...
I wasn't suggesting that the player should hold the arrow keys to
keep the character moving, just that if any key OTHER than an arrow key
is pressed, the character stops moving. i.e. A "dig" key is pressed, the
character digs a hole, and stops beside it. (instead of walking into his
own trap, as it is now)

Thanks! I look forward to your update!

-Mr.X



... "Milhouse, we live in the age of cooties!"  - Bart Simpson
--- Blue Wave/Max v2.12 [NR]
 * Origin: SECRET C::The C Source*HST DS*(403)256-7019,Calgary,Ab (1:134/21)
